from os import environ
from uuid import uuid1
from datetime import datetime

from pynamodb.models import Model
from pynamodb.attributes import UnicodeAttribute, UTCDateTimeAttribute


class Comment(Model):

    class Meta:
        table_name = environ["COMMENTS_TABLE_NAME"]

    parent = UnicodeAttribute(hash_key=True)
    content = UnicodeAttribute(null=False)
    creator = UnicodeAttribute(null=False)

    uuid = UnicodeAttribute(range_key=True, default=lambda: str(uuid1()))
    created = UTCDateTimeAttribute(null=False, default=lambda: datetime.utcnow())
    updated = UTCDateTimeAttribute(null=False, default=lambda: datetime.utcnow())

    def update_content(self, content: str):
        self.content = content
        self.updated = datetime.utcnow()
