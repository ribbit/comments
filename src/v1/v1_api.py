from typing import List

from flask import abort, Blueprint
from flask_restful import Resource, fields, marshal_with
from flask_restful import reqparse, Api

from src.v1.comment import Comment


_COMMENT_FIELDS = dict(
    parent=fields.String,
    uuid=fields.String,
    created=fields.DateTime('iso8601'),
    creator=fields.String,
    updated=fields.DateTime('iso8601'),
    content=fields.String
)


class CommentsV2Handler(Resource):
    CREATE_ARGS = reqparse.RequestParser()
    CREATE_ARGS.add_argument('content', type=str, required=True)
    CREATE_ARGS.add_argument('creator', type=str, required=True)

    @marshal_with(_COMMENT_FIELDS)
    def get(self, parent: str) -> List[Comment]:
        return list(Comment.query(parent))

    @marshal_with(_COMMENT_FIELDS)
    def post(self, parent: str) -> Comment:
        args = self.CREATE_ARGS.parse_args()
        comment = Comment(parent=parent, content=args['content'], creator=args['creator'])
        comment.save()
        return comment


class CommentV2Handler(Resource):

    UPDATE_ARGS = reqparse.RequestParser()
    UPDATE_ARGS.add_argument('content', type=str, required=True)

    @marshal_with(_COMMENT_FIELDS)
    def delete(self, parent: str, uuid: str) -> Comment:
        try:
            comment = Comment.get(parent, uuid)
            comment.delete()
            return comment
        except Comment.DoesNotExist:
            return abort(404)

    @marshal_with(_COMMENT_FIELDS)
    def put(self, parent: str, uuid: str) -> Comment:
        args = self.UPDATE_ARGS.parse_args()
        try:
            comment = Comment.get(parent, uuid)
            comment.update_content(args['content'])
            comment.save()
            return comment
        except Comment.DoesNotExist:
            return abort(404)


V2_BP = Blueprint('v1', __name__)

if True:
    api = Api(V2_BP)
    api.add_resource(CommentsV2Handler,  '/<string:parent>')
    api.add_resource(CommentV2Handler, '/<string:parent>/<string:uuid>')
