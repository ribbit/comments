from flask import Flask
from src.v1.v1_api import V2_BP


APP = Flask(__name__)
APP.register_blueprint(V2_BP, url_prefix='/v1')
