ribbit-comments
===============

The comments service for Ribbit

Requirements
------------

- make
- python3
- npm

Test
----

```bash
$ make init
$ make test
```

Deploy
------

```bash
$ npm i serverless -g
$ npm i
$ sls deploy
```