import pytest
from moto import mock_dynamodb2

from src import APP
from src.v1.comment import Comment


@pytest.fixture(name='client')
def client_fixture():
    APP.config['TESTING'] = True
    APP.config['DEBUG'] = True

    with mock_dynamodb2():
        Comment.create_table(read_capacity_units=1, write_capacity_units=1)
        yield APP.test_client()


@pytest.fixture(name='comment1')
def comment1_fixture() -> Comment:
    comment = Comment(parent='post1', content='this is a comment', creator='user1')
    comment.save()
    return comment


def test_list_missing(client):
    r = client.get('/v1/post1')
    assert r.get_json() == list()


def test_list_populated(client, comment1: Comment):
    response = client.get(f'/v1/{comment1.parent}')
    assert response.status_code == 200

    items = response.get_json()
    assert 1 == len(items)
    item1 = items[0]
    assert item1['parent'] == comment1.parent
    assert item1['uuid'] == comment1.uuid
    assert item1['created']
    assert item1['updated']
    assert item1['content'] == 'this is a comment'
    assert item1['creator'] == comment1.creator


def test_create(client):
    response = client.post(
        'v1/post1',
        data=dict(content='foobarbaz', creator='user1')
    )
    assert response.status_code == 200

    content = response.get_json()
    assert content['parent'] == 'post1'
    assert content['content'] == 'foobarbaz'
    assert content['creator'] == 'user1'
    assert content['created']
    assert content['updated']
    assert content['uuid']


def test_create_invalid(client):
    response = client.post(f'/v1/post1', data=dict())
    assert response.status_code == 400


def test_delete_missing(client):
    response = client.delete('/v1/post1/comment1')
    assert response.status_code == 404


def test_delete(client, comment1: Comment):
    response = client.delete(f'/v1/{comment1.parent}/{comment1.uuid}')
    assert response.status_code == 200
    assert response.get_json()['uuid'] == comment1.uuid


def test_update_not_found(client):
    response = client.put('/v1/post1/comment1', data=dict(content='lolcats'))
    assert response.status_code == 404


def test_update(client, comment1: Comment):
    response = client.put(
        f'/v1/{comment1.parent}/{comment1.uuid}',
        data=dict(content='lolcats'))
    assert response.status_code == 200
    updated = response.get_json()
    assert updated['content'] == 'lolcats'
    assert updated['updated'] > updated['created']


def test_update_invalid(client, comment1):
    response = client.put(
        f'/v1/{comment1.parent}/{comment1.uuid}',
        data=dict())
    assert response.status_code == 400
